﻿namespace HomeWork_05
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberHelper.NumberHelperRun();
            MatrixHelper.MatrixHelperRun();
            StringHelper.StringHelperRun();
            AckermannFunctionHelper.AckermannFunctionRun();
        }
    }
}